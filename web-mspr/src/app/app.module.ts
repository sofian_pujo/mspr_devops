import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {MatSelectModule} from '@angular/material/select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule, MatListModule, MatButtonModule, MatMenuModule} from '@angular/material';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';
import { CalculComponent } from './features/calcul/calcul/calcul.component';
import { AgmCoreModule } from '@agm/core';
import { MapsComponent } from './features/maps/maps/maps.component';
import { HomeComponent } from './features/home/home/home.component';
import { FormComponent } from './features/form/form/form.component';
import {google} from 'google-maps';
import {MatDialogModule} from '@angular/material/dialog';
import { ModalComponent } from './features/maps/maps/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculComponent,
    MapsComponent,
    HomeComponent,
    FormComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatSelectModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatCardModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    LayoutModule,
    MatButtonModule,
    MatMenuModule,
    RouterModule,
    MatCardModule,
    AgmCoreModule,
    MatDialogModule,
    MatIconModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBgN_hR8nnwyN_MO3UED1iJItfb5CxRLjg'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
declare var google : google;