import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalculComponent } from './calcul/calcul.component';

const routes: Routes = [  {
  path: "",
  component: CalculComponent,
  pathMatch: "full"
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalculRoutingModule { }
