import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculComponent } from './calcul/calcul.component';

@NgModule({
  declarations: [CalculComponent],
  imports: [
    CommonModule
  ]
})
export class CalculModule { }
