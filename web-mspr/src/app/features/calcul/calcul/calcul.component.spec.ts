import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculComponent } from './calcul.component';
describe('calculs' , () => {
  let CC = new CalculComponent();
  it('calculatenet1', () => {
    CC.calculatenet(10,0.2);
    expect(CC.sumnet).toEqual(8);
  });
  it('calculatenet2', () => {
    CC.calculatenet(-1000,0.2);
    expect(CC.sumnet).toEqual(-800);
  });
  it('calculatenet3', () => {
    CC.calculatenet(-2354,-43);
    expect(CC.sumnet).toEqual(-103576);
  });
  it('calculatenet4', () => {
    CC.calculatenet(7.7,0.3);
    expect(CC.sumnet).toEqual(5.39);
  });
  it('calculateremise1', () => {
    CC.calculateremise(5,10);
    expect(CC.sumremise).toEqual(50);
  });
  it('calculateremise2', () => {
    CC.calculateremise(-1000,-800);
    expect(CC.sumremise).toEqual(-25);
  });
  it('calculateremise3', () => {
    CC.calculateremise(-2354,103576);
    expect(CC.sumremise).toEqual(102);
  });
  it('calculateremise4', () => {
    CC.calculateremise(0.3,0.7);
    expect(CC.sumremise).toEqual(57);
  });
  it('calculatevente1', () => {
    CC.calculatevente(0.4,10);
    expect(CC.sumvente).toEqual(4);
  });
  it('calculatevente2', () => {
    CC.calculatevente(-1.2,800);
    expect(CC.sumvente).toEqual(-960);
  });
  it('calculatevente3', () => {
    CC.calculatevente(1.2,-800);
    expect(CC.sumvente).toEqual(-960);
  });
  it('calculatevente4', () => {
    CC.calculatevente(0.4,0.3);
    expect(CC.sumvente).toEqual(0);
  });
  it('calculatecoef1', () => {
    CC.calculatecoef(25,20);
    expect(CC.sumcoef).toEqual(1.25);
  });
  it('calculatecoef2', () => {
    CC.calculatecoef(1000,-200);
    expect(CC.sumcoef).toEqual(-5);
  });
  it('calculatecoef3', () => {
    CC.calculatecoef(-1000,200);
    expect(CC.sumcoef).toEqual(-5);
  });
  it('calculatecoef4', () => {
    CC.calculatecoef(0.6,0.3);
    expect(CC.sumcoef).toEqual(2);
  });
})
