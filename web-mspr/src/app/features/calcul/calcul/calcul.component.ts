import { Component, OnInit } from '@angular/core';

export interface Calcul {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-calcul',
  templateUrl: './calcul.component.html',
  styleUrls: ['./calcul.component.css']
})
export class CalculComponent implements OnInit {

  calculs: Calcul[] = [
    {value: 'achat-0', viewValue: 'Prix d’achat net'},
    {value: 'remise-1', viewValue: 'Taux de remise'},
    {value: 'vente-2', viewValue: 'Prix de vente net'},
    {value: 'coef-2', viewValue: 'Coefficient multiplicateur'}
  ];
  sumnet : number;
  calculatenet(netbrut:number, netremise:number) {
    this.sumnet = +netbrut *(1 - +netremise);
   }

   sumremise : number;
  calculateremise(remisenet:number, remisebrut:number) {
    this.sumremise = Math.trunc((1 - +remisenet / +remisebrut)*100) ;
   }

   sumvente : number;
  calculatevente(ventenet:number, ventecoef:number) {
    this.sumvente = Math.trunc(+ventenet * +ventecoef);
   }

   sumcoef : number;
  calculatecoef(coefvente:number, coefachat:number) {
    this.sumcoef = +coefvente / +coefachat ;
   }

  ngOnInit() {
  }

}
