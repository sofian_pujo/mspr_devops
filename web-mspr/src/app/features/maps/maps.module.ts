import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsComponent } from './maps/maps.component';
import {MatCardModule} from '@angular/material/card';
import { ModalComponent } from './maps/modal/modal.component';

@NgModule({
  declarations: [MapsComponent, ModalComponent],
  imports: [
    CommonModule,
    MatCardModule
  ]
})
export class MapsModule { }
