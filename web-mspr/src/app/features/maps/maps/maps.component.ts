import { Component, OnInit } from '@angular/core';
import { google } from 'google-maps';
import {MatDialogModule, MatDialog} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { ModalComponent } from './modal/modal.component';


@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css'],
  styles: []
})
export class MapsComponent implements OnInit {

  latitude: number = 43.643219;
  longitude: number = 3.838371;

  pharmacie_trifontaine_lattitude = 43.6489986;
  pharmacie_trifontaine_longitude = 3.8415083;
  pharmacie_croix_rousse_lattitude = 43.6414197;
  pharmacie_croix_rousse_longitude = 3.8455801;
  pharmacie_pierre_boissier_lattitude = 43.64815218356966;
  pharmacie_pierre_boissier_longitude = 3.8317126035690308;
  pharmacie_o_lattitude = 43.633671883391905;
  pharmacie_o_longitude = 3.843744993209839;
  pharmacie_malbosc_lattitude = 43.633237032205415;
  pharmacie_malbosc_longitude = 3.8330644369125366;
  pharmacie_europe_lattitude = 43.6419800426947;
  pharmacie_europe_longitude = 3.81999671459198;

  lat:any;
  lng:any;

  constructor(public dialog: MatDialog){
    if (navigator)
    {
    navigator.geolocation.getCurrentPosition( pos => {
        this.longitude = +pos.coords.longitude;
        this.latitude = +pos.coords.latitude;
      });
    }
  }
  animal: string;
  name: string;

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px'
    });
  }

  ngOnInit() {
  }

}
