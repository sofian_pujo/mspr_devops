import { Component, OnInit } from '@angular/core';
import { isArray } from 'util';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  constructor() { }

  field_elements:Array<any>;

  getAllElements(form_elements) {
    form_elements.forEach(element => {
      if(element.hasChildNodes()) {
        this.getAllElements(element.childNodes);
      } else {
        if (element.id != null) {
          if (element.id.match("-field$") != null) {
            console.log(element);
            var btn_create = document.createElement("BUTTON");   // Create a <button> element
            var btn_remove = document.createElement("BUTTON");   // Create a <button> element
            btn_create.innerHTML = "+";
            btn_remove.innerHTML = "-"
            element.parentNode.parentNode.appendChild(btn_create);
            element.parentNode.parentNode.appendChild(btn_remove);
          }
        }
      }
    });
  }

  ngOnInit() {
    var form_elements = document.getElementById("div_form").childNodes;
    //this.getAllElements(form_elements);
    //$("#change-form").on("click", this.getAllElements(form_elements));
    //console.log(form_elements);
    //document.getElementById("change-form").addEventListener("click", );
  }
}
