import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalculComponent } from './features/calcul/calcul/calcul.component';
import { AppComponent } from './app.component';
import { MapsComponent } from './features/maps/maps/maps.component';
import { HomeComponent } from './features/home/home/home.component';
import { FormComponent } from './features/form/form/form.component';
import { ModalComponent } from './features/maps/maps/modal/modal.component';

const appRoutes: Routes = [
  {
    path: "calcul",
    component: CalculComponent
  },
  {
    path: "maps",
    component: MapsComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "form",
    component: FormComponent
  },
  {
    path: "modal",
    component: ModalComponent
  },
  {path: '' , redirectTo:'/home',pathMatch:'full'}
  ];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
