import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Pharmacie } from '../model/pharmacie';
import { GlobalConstants } from './global-constants';

@Injectable({
  providedIn: 'root'
})
export class PharmacieService {
  private baseUrl = "http://127.0.0.1/mspr-back/back.php?code=1";

  private cachePharmacie: Observable<Array<Pharmacie>>;

  constructor(private http: HttpClient) { }

  getPharmacieCached() {
    if (!this.cachePharmacie) {
      this.cachePharmacie = this.findAll().pipe(
        shareReplay(GlobalConstants.cacheSize)
      );
    }

    return this.cachePharmacie;
  }

  findAll(): Observable<any> {
    return this.http.get(this.baseUrl + "/");
  }

  // update(pharmacie: Pharmacie): Observable<any> {
  //   return this.http.put(this.baseUrl + "/", pharmacie);
  // }
}
