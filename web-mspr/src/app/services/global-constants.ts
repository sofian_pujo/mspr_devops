

export class GlobalConstants {
   static snackbarDuration: number = 5000;
   static snackbarDurationInfo = 10000; 

   static cacheSize: 1;

   static namePattern: "[a-zA-Z0-9- ]*";
}
