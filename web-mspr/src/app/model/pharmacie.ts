export class Pharmacie {
    constructor(
        public id: number,
        public nom: string,
        public adresse: string,
        public x: number,
        public y: number,
        public tel: string) { }
}